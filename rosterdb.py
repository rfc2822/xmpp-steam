# license: public domain (see LICENSE)

import logging
import pickle

class RosterDB:

    logger = logging.getLogger('RosterDB')
    data = {}

    def __init__(self):
        try:
            with open('roster.db', 'rb') as handle:
                self.data = pickle.load(handle)
        except: return
        self.logger.debug("Loaded %i roster entries" % len(self.data))

    def entries(self, owner_jid, db_state=None):
        self.logger.debug("Enumerating roster items of %s" % owner_jid)
        if owner_jid is None:
            return self.data.keys()
        else:
            return self.data.get(owner_jid, [])

    def load(self, owner_jid, jid, db_state):
        entries = self.data.get(owner_jid, {})
        result = entries.get(jid, {})
        self.logger.debug("Loaded roster item of %s: %s = %s" % (owner_jid, jid, result))
        return result

    def save(self, owner_jid, jid, item, db_state):
        self.logger.debug("Saving roster item of %s: %s = %s" % (owner_jid, jid, item))
        entries = self.data.get(owner_jid, {})
        entries[jid] = item
        self.data[owner_jid] = entries
        
        with open('roster.db', 'wb') as handle:
            pickle.dump(self.data, handle, -1)
