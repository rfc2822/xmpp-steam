#!/usr/bin/env python
# -*- coding: utf-8 -*-
# license: public domain (see LICENSE)

from gevent import monkey
monkey.patch_all()

import sys
import logging
import getpass
from optparse import OptionParser

import sleekxmpp
from sleekxmpp import Iq
from sleekxmpp.componentxmpp import ComponentXMPP
from sleekxmpp.xmlstream import register_stanza_plugin
from sleekxmpp.xmlstream.handler.callback import Callback
from sleekxmpp.plugins.xep_0030.stanza.info import DiscoInfo
from steam.enums import EPersonaState, EChatEntryType

from rosterdb import RosterDB
from steamthread import SteamThread
from usermanager import UserManager

# Python versions before 3.0 do not use UTF-8 encoding
# by default. To ensure that Unicode is handled properly
# throughout SleekXMPP, we will set the default encoding
# ourselves to UTF-8.
if sys.version_info < (3, 0):
    reload(sys)
    sys.setdefaultencoding('utf8')
else:
    raw_input = input


class SteamComponent(ComponentXMPP):

    logger = logging.getLogger('SteamComponent')

    user_manager = UserManager()
    steam = {
        # "jid": SteamThread for user jid
    }

    def __init__(self, jid, secret, server, port):
        ComponentXMPP.__init__(self, jid, secret, server, port)

        self.roster.set_backend(RosterDB())

        self.gateway_jid = self.boundjid.full

        self.auto_authorize = True
        self.auto_subscribe = True
        self.add_event_handler("presence_unsubscribed", self.presence_unsubscribed)

        self.registerPlugin('xep_0077', module='registration')
        self['xep_0077'].set_user_manager(self.user_manager)

        self.add_event_handler("message", self.message)
        self.add_event_handler("got_online", self.got_online)
        self.add_event_handler("got_offline", self.got_offline)
        self.add_event_handler("changed_status", self.changed_status)
        self.add_event_handler("chatstate", self.chatstate)

    def presence_unsubscribed(self, presence):
        if (presence['to'] == self.gateway_jid):
            self.user_manager.remove(presence['from'].bare)

    def message(self, msg):
        if (msg.getTo().full == self.gateway_jid):
            # message to transport
            request = msg['body']
            reply = None
            steam = self.steam.get(msg.getFrom().bare, None)
            if request == "help":
                reply = "help, reconnect, restart, status"
            elif request == "reconnect":
                if steam:
                    steam.client.reconnect()
                    reply = "Reconnecting"
                else:
                    reply = "No steam client, go online first"
            elif request == "restart":
                steam = self.steam.get(msg.getFrom().bare, None)
                if steam != None:
                    steam.client.logout()
                jid = msg['from'].bare
                steam = SteamThread(self, jid)
                self.steam[jid] = steam
                reply = "Client created and started"
            elif request == "status":
                if steam:
                    reply = ("Connected: %s" % "yes" if steam.client.connected else "no") \
                        + ("\nLogged on: %s" % "yes" if steam.client.logged_on else "no") \
                        + ("\nRelogin available: %s" % "yes" if steam.client.relogin_available else "no") \
                        + ("\nUser name: %s" % steam.client.user.name) \
                        + ("\nUser state: %s" % steam.client.user.state)
                else:
                    reply = "No steam client, go online first"
            else:
                reply = "Unknown command. Try 'help'"
            msg = msg.reply()
            msg['body'] = reply
            msg.send()
        else:
            self.steam[msg.getFrom().bare].send_message(msg.getTo().user, msg['body'])

    def chatstate(self, msg):
        self.steam[msg.getFrom().bare].send_chat_state(msg.getTo().user, msg['chat_state'])

    def got_online(self, presence):
        if (presence['to'] == self.gateway_jid):
            jid = presence['from'].bare
            steam = self.steam.get(jid, None)
            if steam == None:
                self.logger.info("Starting Steam client")
                steam = SteamThread(self, jid)
            self.steam[jid] = steam

    def got_offline(self, presence):
        if (presence['to'] == self.gateway_jid):
            jid = presence['from'].bare
            steam = self.steam.pop(jid, None)
            if steam != None:
                steam.logout()
                self.logger.info("Stopping Steam client")

    def changed_status(self, presence):
        self.logger.debug("USER CHANGED STATUS: ", presence)
        if (presence['to'] == self.gateway_jid):
            jid = presence['from'].bare
            steam = self.steam.get(jid, None)
            if steam != None:
                resources = self.client_roster[jid].resources
                show = self.merged_presence(resources)
                steam.change_status(show)

    def merged_presence(self, resources):
        show = "_unavailable"
        prio = None
        for res in resources.values():
            if prio is None or res['priority'] > prio:
                show = res['show']
                prio = res['priority']
        return show

    def get_info(self, jid=None, node=None, local=None, cached=None, **kwargs):
        info = DiscoInfo()
        if (jid.bare.endswith("@%s" % self.gateway_jid)):
            info.add_feature('http://jabber.org/protocol/chatstates')
        return info


if __name__ == '__main__':
    optp = OptionParser()
    optp.add_option('-q', '--quiet', help='set logging to ERROR',
                    action='store_const', dest='loglevel',
                    const=logging.ERROR, default=logging.INFO)
    optp.add_option('-d', '--debug', help='set logging to DEBUG',
                    action='store_const', dest='loglevel',
                    const=logging.DEBUG, default=logging.INFO)
    optp.add_option('-v', '--verbose', help='set logging to COMM',
                    action='store_const', dest='loglevel',
                    const=5, default=logging.INFO)

    optp.add_option("-j", "--jid", dest="jid",
                    help="JID to use")
    optp.add_option("-p", "--password", dest="password",
                    help="password to use")
    optp.add_option("-s", "--server", dest="server",
                    help="server to connect to")
    optp.add_option("-P", "--port", dest="port",
                    help="port to connect to")

    opts, args = optp.parse_args()

    if opts.jid is None or opts.password is None:
        print "Must specify user name/password"
        sys.exit(1)
    if opts.server is None:
        opts.server = "localhost"
    if opts.port is None:
        opts.port = 5347

    logging.basicConfig(level=opts.loglevel,
                        format='%(levelname)-8s %(message)s')

    xmpp = SteamComponent(opts.jid, opts.password, opts.server, opts.port)

    xmpp.registerPlugin('xep_0030') # Service Discovery
    disco = xmpp['xep_0030']
    disco.add_identity(category='gateway',
        itype="xmpp",
        jid=xmpp.boundjid.full,
        name="Steam Transport"
    )
    xmpp['xep_0030'].api.register(xmpp.get_info, "get_info")

    # xmpp.registerPlugin('xep_0153') # vCard
    xmpp.registerPlugin('xep_0085') # Chat State Notifications
    xmpp.registerPlugin('xep_0199') # XMPP Ping

    # Connect to the XMPP server and start processing XMPP stanzas.
    if xmpp.connect():
        xmpp.process(block=True)
        print("Done")
    else:
        print("Unable to connect.")

