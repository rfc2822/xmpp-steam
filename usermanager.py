# license: public domain (see LICENSE)

import logging
import pickle

class UserManager:

    logger = logging.getLogger('UserManager')
    users = {
        # "bare@jid": { "username": "xxx", "password": "xx", "auth_code": "xx" }
    }

    def __init__(self):
        self.load()
        self.logger.info("Loaded %i user(s)" % len(self.users))

    def register(self, bare_jid, username, password, auth_code):
        user = {
            "username": username,
            "password": password,
            "auth_code": auth_code
        }
        self.users[bare_jid] = user
        self.save()
        self.logger.info("Added user %s (%s)" % (bare_jid, user))
        return True

    def remove(self, bare_jid):
        self.users.pop(bare_jid, None)
        self.save()
        self.logger.info("Removed user %s" % bare_jid)

    def get(self, bare_jid):
        return self.users.get(bare_jid, None)

    def has_user(self, bare_jid):
        return self.get(bare_jid) != None


    def load(self):
        try:
            with open('users.db', 'rb') as handle:
                self.users = pickle.load(handle)
        except: return

    def save(self):
        with open('users.db', 'wb') as handle:
            pickle.dump(self.users, handle, -1)
        self.logger.info("Saved %i user(s)" % len(self.users))

