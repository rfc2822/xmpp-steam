# license: public domain (see LICENSE)

from steam import SteamClient, SteamID
from steam.core.msg import MsgProto
from steam.enums import EPersonaState, EChatEntryType
from steam.enums.emsg import EMsg

import urllib

class SteamThread:

    shutdown = False

    known_games = {}

    def __init__(self, xmpp, jid):
        self.xmpp = xmpp
        self.jid = jid

        client = SteamClient()
        client.set_credential_location(".")
        self.client = client

        self.client.on("error", self.on_error)
        self.client.on("channel_secured", self.on_channel_secured)
        self.client.on("disconnected", self.on_disconnected)
        self.client.on("logged_on", self.on_logged_on)

        self.client.on("chat_message", self.on_chat_message)
        self.client.on(EMsg.ClientPersonaState, self.on_friend_changed_state)
        self.client.on(EMsg.ClientFriendMsgIncoming, self.on_chat_message_incoming)
        self.client.friends.on("ready", self.on_friends_ready)

        credentials = xmpp.user_manager.get(jid)
        client.login(**credentials)

    def logout(self):
        self.shutdown = True
        self.client.logout()

    def change_status(self, show):
        state = {
            '_unavailable': EPersonaState.Offline,
            '': EPersonaState.Online,
            'away': EPersonaState.Snooze,
            'chat': EPersonaState.LookingToPlay,
            'dnd': EPersonaState.Busy,
            'xa': EPersonaState.Away
        }[show]
        self.client.change_status(persona_state=state)

    def send_message(self, to_id, message):
        recipient = self.client.get_user(to_id, False)
        recipient.send_message(message)

    def send_chat_state(self, to, chat_state):
        state = EChatEntryType.Invalid
        if chat_state == 'composing':
            state = EChatEntryType.Typing
        elif chat_state == 'gone':
            state = EChatEntryType.LeftConversation
        self.client.send(MsgProto(EMsg.ClientFriendMsg), {
            'steamid': SteamID(to),
            'chat_entry_type': state
        })

    
    def on_error(self, error):
        msg = str(error)
        self.xmpp.send_message(mfrom=self.xmpp.gateway_jid, mto=self.jid, mbody=msg)
        self.send_gateway_status(error=msg)

    def on_channel_secured(self):
        if not self.shutdown and self.client.relogin_available:
            self.client.relogin()
        self.send_gateway_status()

    def on_disconnected(self):
        self.send_gateway_status()
        if self.client.relogin_available:
            self.client.reconnect(maxdelay=30)
        else:
            credentials = self.xmpp.user_manager.get(jid)
            self.client.login(**credentials)

    def on_logged_on(self):
        self.send_gateway_status()


    def on_friends_ready(self):
        for friend in self.client.friends:
            jid = "%s@%s" % (friend.steam_id, self.xmpp.gateway_jid)
            self.xmpp.send_presence_subscription(pfrom=jid, pto=self.jid, pnick=friend.name)

            avatar_url = friend.get_avatar_url(size=1)
            stream = urllib.urlopen(avatar_url)
            avatar = stream.read()
            stream.close()

            #self.xmpp['xep_0153'].set_avatar(jid=jid, avatar=avatar, mtype="image/jpeg")
            self.xmpp.send_presence(pfrom=jid, pto=self.jid, pnick=friend.name, **self.presence_from_state(friend.state))

    def on_chat_message(self, user, message):
        jid = "%s@%s" % (user.steam_id, self.xmpp.gateway_jid)
        self.xmpp.send_message(mfrom=jid, mto=self.jid, mbody=message, mtype='chat')

    def on_chat_message_incoming(self, message):
        jid = "%s@%s" % (message.body.steamid_from, self.xmpp.gateway_jid)
        notification = self.xmpp.make_message(self.jid, mfrom=jid, mtype='chat')
        if (message.body.chat_entry_type == 1):
            notification['chat_state'] = 'active'
        elif (message.body.chat_entry_type == 2):
            notification['chat_state'] = 'composing'
        elif (message.body.chat_entry_type == 6):
            notification['chat_state'] = 'gone'
        notification.send()

    def on_friend_changed_state(self, message):
        for friend in message.body.friends:
            jid = "%s@%s" % (friend.friendid, self.xmpp.gateway_jid)
            state = EPersonaState(friend.persona_state)
            self.xmpp.send_presence(pfrom=jid, pto=self.jid, pnick=friend.player_name, **self.presence_from_state(state, friend.gameid))


    def get_game(self, game_id):
        game = self.known_games.get(game_id, None)
        if game != None: return game

        result = self.client.get_product_info([game_id])
        game = result['apps'][game_id]
        self.known_games[game_id] = game
        return game

    def presence_from_state(self, state, game_id=0):
        pshow = None
        ptype = None
        if state == EPersonaState.Busy:
            pshow = "dnd"
        elif state == EPersonaState.Away:
            pshow = "xa"
        elif state == EPersonaState.Snooze:
            pshow = "away"
        elif state in [EPersonaState.LookingToTrade, EPersonaState.LookingToPlay]:
            pshow = "chat"
        elif state == EPersonaState.Offline:
            ptype = "unavailable"
        info = { "ptype": ptype, "pshow": pshow }

        if game_id != 0:
            game = self.get_game(game_id)
            info["pstatus"] = "Playing %s" % game['common']['name']

        return info

    def send_gateway_status(self, error=None):
        if self.client.logged_on:
            status = ""
        else:
            status = "xa"

        presence = { 'pto': self.jid, 'pshow': status }
        if error != None:
            presence['pstatus'] = error

        self.xmpp.send_presence(**presence)
