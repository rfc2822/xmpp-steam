# license: public domain (see LICENSE)

from sleekxmpp import Iq
from sleekxmpp.plugins.base import base_plugin
from sleekxmpp.xmlstream import ElementBase, ET, JID, register_stanza_plugin
from sleekxmpp.xmlstream.handler.callback import Callback
from sleekxmpp.xmlstream.matcher.xpath import MatchXPath
from sleekxmpp.plugins.xep_0004 import Form

class Register(ElementBase):
    namespace = 'jabber:iq:register'
    name = 'query'
    plugin_attrib = 'register'

    #interfaces = set(('instructions'))
    #sub_interfaces = interfaces

    def defaultForm(self):
        form = self['form']
        form.add_field(var='username', label='User name',required=True)
        form.add_field(var='password', label='Password',required=True)
        form.add_field(var='auth_code', label='Auth code',required=False)


class xep_0077(base_plugin):
    """
    XEP-0077 In-Band Registration
    """

    description = "In-Band Registration"
    xep = "0077"
    dependencies = set(['xep_0004'])

    user_manager = None


    def plugin_init(self):
        self.xmpp.register_handler(Callback(
            'In-Band Registration',
            MatchXPath('{%s}iq/{jabber:iq:register}query' % self.xmpp.default_ns),
            self.handle_register
        ))

        register_stanza_plugin(Iq, Register)
        register_stanza_plugin(Register, self.xmpp['xep_0004'].stanza.Form)

    def post_init(self):
        base_plugin.post_init(self)
        self.xmpp['xep_0030'].add_feature("jabber:iq:register")


    def set_user_manager(self, user_manager):
        self.user_manager = user_manager


    def handle_register(self, iq):
        sender = iq['from'].bare
        if iq['type'] == 'get':
            self.sendRegistrationForm(iq)
        elif iq['type'] == 'set':
            if iq['register']['remove']:
                iq = iq.reply()
                iq.error()
                iq.send()
                return

            data = iq['register']['form'].get_values()
            if not data.get('username', None) or not data.get('password', None):
                iq = iq.reply()
                iq.error()
                iq.send()
                return

            if self.user_manager.register(sender, **data):
                # user registered successfully
                iq.reply().send()

                # request authorization
                # self.xmpp.sendPresenceSubscription(pfrom=self.xmpp.gateway_jid, pto=sender)

            else:
                iq = iq.reply()
                iq.error()
                iq.send()
            

    def sendRegistrationForm(self, iq):
        reg = iq['register']
        reg.defaultForm()
        iq.reply().setPayload(reg.xml)
        iq.send()
